
const descripcion={
    demand: true,
    alias: 'd',
    desc: 'descripción de la tarea por hacer'
};

const completado = {
    alias: 'c',
    default: true,
    desc: 'Marca completada o pendiente'


}

const filtro = {
    alias: 'f',
    desc: 'Filtrar completo o no'
}
const argv = require('yargs')
    .command('crear', 'crear elemento por hacer', {
        descripcion
    })
    .command('borrar', 'borra elemento de la lista', {
        descripcion
    })
    .command('actualizar', 'Actualiza el estado completado de una tarea', {
        descripcion,
    completado
})
.command('listar', 'lista las tareas completas', {

filtro

})
    .help()
    .argv;

module.exports = {
    argv
}